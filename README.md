The Shepherd of the Hills Outdoor Drama is the action-packed live reenactment of the famous historical novel by Christian minister, Harold Bell Wright. Call (417) 334-4191 for more information!

Address: 5586 W Missouri 76, Branson, MO 65616, USA

Phone: 417-334-4191
